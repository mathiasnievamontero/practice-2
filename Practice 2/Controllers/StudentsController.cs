﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Practice_2.Controllers
{
    [ApiController]
    [Route("/")]
    public class StudentsController : ControllerBase
    {
        public StudentsController()
        {
        }

        [HttpGet]
        public List<Student> GetStudents()
        {
            var studentList = new List<Student>();
            var student1 = new Student(); 
            var student2 = new Student();
            var student3 = new Student();
            var student4 = new Student();

            student1.Name = "Pepe";
            student2.Name = "Carl";
            student3.Name = "Jose";
            student4.Name = "Jim";

            studentList.Add(student1);
            studentList.Add(student2);
            studentList.Add(student3);
            studentList.Add(student4);

            return studentList; 
        }
        
        [HttpPost]
        public Student CreateStudent([FromBody]string studentName)
        {
            return new Student() { Name = studentName }; 
        }

        [HttpPut]
        public Student UpdateStudent([FromBody]Student student)
        {
            student.Name = "updated";
            return student; 
        }

        [HttpDelete]
        public Student DeleteStudent([FromBody]Student student)
        {
            student.Name = "Deleted";
            return student; 
        }
    }
}
